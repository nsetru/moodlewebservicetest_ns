<?php

require('../../config.php');
require_once($CFG->dirroot . '/blocks/kaplan_plugin/curl.php');

require_login();
$courseid = required_param('courseid', PARAM_INT); //if no courseid is given
$functionname = optional_param('function', 'getusers', PARAM_TEXT);

global $DB, $OUTPUT;

$context = context_course::instance($courseid);
$PAGE->set_url('/blocks/kaplan_plugin/test_client.php');
$PAGE->set_context($context);
$PAGE->set_heading($SITE->fullname);
$PAGE->set_pagelayout('course');
$PAGE->set_title('Test Web Service - REST');

// Initialise Curl class that has all REST related functions
$curl = new curlRest();

echo $OUTPUT->header();

// $token = 'b36c6bef0019e95a1fd5116cd2d0dcdd';

//check if the token is configured for this plugin
if(empty($CFG->block_kaplan_plugin_token)) {
   exit;
}

// Get the token from kaplan_plugin settings page
$token = $CFG->block_kaplan_plugin_token;
$domainname = $CFG->wwwroot;
// REST RETURNED VALUES FORMAT
$restformat = 'json'; //Also possible in Moodle 2.2 and later: 'json'
$restformat = ($restformat == 'json')?'&moodlewsrestformat=' . $restformat:'';

// We are not passing any parameters for both functions
$params = array();

// Display HTML table for list of users
if($functionname === 'getusers') {

    echo $OUTPUT->heading(get_string('listusers', 'block_kaplan_plugin'));
    echo $OUTPUT->box_start('generalbox boxaligncenter boxwidthwide');

    // Define external function in Moodle API
    $functionname = 'block_kaplan_plugin_get_users_custom';

    /// REST CALL
    $serverurl = $domainname . '/webservice/rest/server.php'. '?wstoken=' . $token . '&wsfunction='.$functionname;
    $respusers = $curl->get($serverurl . $restformat, $params);

    // Decode json response
    $users = json_decode($respusers);

    $table = new html_table();
    $table->width = "95%";
    $table->head = array('user ID', 'user fullname');
    $columns = array('user ID','user fullname');

    $users = $users->users;
    foreach ($users as $user) {

        $table->data[] = array (
            $user->userid,
            $user->userfullname
        );
    }

    echo html_writer::table($table);
}

// Display HTML table for list of courses
if($functionname === 'getcourses') {
    echo $OUTPUT->heading(get_string('listcourses', 'block_kaplan_plugin'));
    echo $OUTPUT->box_start('generalbox boxaligncenter boxwidthwide');

    // Define external function in Moodle API
    $functionname = 'block_kaplan_plugin_get_courses_custom';

    /// REST CALL
    $serverurl = $domainname . '/webservice/rest/server.php'. '?wstoken=' . $token . '&wsfunction='.$functionname;
    $respcourses = $curl->get($serverurl . $restformat, $params);

    // Decode json response
    $courses = json_decode($respcourses);

    $table = new html_table();
    $table->width = "95%";
    $table->head = array('Course ID', 'Course Name', 'Number of users enrolled');
    $columns = array('Course ID', 'Course Name', 'Number of users enrolled');

    $courses = $courses->courses;
    foreach ($courses as $course) {

        $table->data[] = array (
            $course->courseid,
            $course->coursename,
            $course->enrolledusers
        );
    }

    echo html_writer::table($table);
}

echo $OUTPUT->box_end();
echo $OUTPUT->footer();
