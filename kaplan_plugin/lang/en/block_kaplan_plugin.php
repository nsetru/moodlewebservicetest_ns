<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_kaplan_plugin', language 'en'
 *
 * @package   block_kaplan_plugin
 * @copyright Nivedita Setru <nsetru@gmail.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['blockstring'] = 'Block string';
$string['descconfig'] = 'Description of the config section';
$string['descfoo'] = 'Config description';
$string['headerconfig'] = 'Config section header';
$string['labelfoo'] = 'Config label';
$string['kaplan_plugin:addinstance'] = 'Add a kaplan_plugin block';
$string['kaplan_plugin:myaddinstance'] = 'Add  block to my moodle';
$string['token'] = 'Token';
$string['tokeninfo'] = 'Web services token relevant to this plugin';
$string['listusers'] = 'List of users';
$string['listcourses'] = 'List of courses';
$string['pluginname'] = 'Kaplan Plugin';
