<?php

/**
 * Kaplan plugin external functions and service definitions.
 *
 * @package    block_kaplan_plugin
 * @copyright  2015 Nivedita Setru <n.setru@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$functions = array(

    'block_kaplan_plugin_get_users_custom' => array(
        'classname' => 'block_kaplan_plugin_external',
        'methodname' => 'get_users_custom',
        'classpath' => 'blocks/kaplan_plugin/externallib.php',
        'description' => 'Returns list of users',
        'type' => 'read'
    ),

    'block_kaplan_plugin_get_courses_custom' => array(
        'classname'   => 'block_kaplan_plugin_external',
        'methodname'  => 'get_courses_custom',
        'classpath'   => 'blocks/kaplan_plugin/externallib.php',
        'description' => 'Returns list of courses',
        'type'        => 'read',
    )
);