<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * External Web Service
 *
 * @package    block_kaplan_plugin
 * @copyright  2015 Nivedita Setru <n.setru@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once($CFG->libdir . "/externallib.php");
class block_kaplan_plugin_external extends external_api {

    /**
     * Returns description of get_users_custom parameters
     * @return external_function_parameters
     */
    public static function get_users_custom_parameters() {
        return new external_function_parameters(
            array(
                // No parameters to define here
            )
        );
    }

    /**
     * Return users Moodle
     *
     * @return mixed
     * @throws invalid_parameter_exception
     */
    public static function get_users_custom() {
        global $DB;

        // Parameter validation.
        $params = self::validate_parameters(self::get_users_custom_parameters(), array());

        $users = $DB->get_records('user', array('deleted' => 0), 'id ASC', 'id, username, firstname, lastname');

        $formatteduser = array();
        foreach ($users as $user) {
            $returnuser['userid'] = $user->username;
            $returnuser['userfullname'] = fullname($user);

            $formatteduser[] = $returnuser;
        }

        $formattedusers['users'] = $formatteduser;

        return $formattedusers;
    }

    /**
     * Returns description of get_users_custom result value
     * @return external_description
     */
    public static function get_users_custom_returns() {
        return new external_function_parameters(
            array(
                'users' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'userid' => new external_value(PARAM_RAW, 'User Id'),
                            'userfullname' => new external_value(PARAM_TEXT, 'User Fullname')
                        )
                    )
                )
            )
        );
    }

    /**
     * Returns description of get_courses_custom parameters
     * @return external_function_parameters
     */
    public static function get_courses_custom_parameters() {
        return new external_function_parameters(
            array(
                // No parameters to define here
            )
        );
    }

    /**
     * Return courses Moodle
     * 
     * @return mixed
     * @throws invalid_parameter_exception
     */
    public static function get_courses_custom() {
        global $DB;

        // Parameter validation.
        $params = self::validate_parameters(self::get_courses_custom_parameters(), array());

        $courses = $DB->get_records('course', array(), 'id ASC', 'id, fullname, shortname');

        // declare an empty array
        $formattedcourse = array();
        foreach ($courses as $course) {
            $context = context_course::instance($course->id);
            $enrolleduserscount = count_enrolled_users($context);

            $returncourse['courseid'] = $course->id;
            $returncourse['coursename'] = $course->fullname;
            $returncourse['enrolledusers'] = $enrolleduserscount;

            $formattedcourse[] = $returncourse;
        }

        $formattedcourses['courses'] = $formattedcourse;

        return $formattedcourses;

    }

    /**
     * Returns description of get_courses_custom result value
     * @return external_description
     */
    public static function get_courses_custom_returns() {
        return new external_function_parameters(
            array(
                'courses' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'courseid' => new external_value(PARAM_INT, 'Course Id'),
                            'coursename' => new external_value(PARAM_TEXT, 'Course fullname'),
                            'enrolledusers' => new external_value(PARAM_INT, 'Number of users enrolled')
                        )
                    )
                )
            )
        );
    }
}