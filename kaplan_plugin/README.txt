Overview of plugin

- Download and install the plugin under /blocks/
- Navigate to site administration >> Notifications page
- It should ask for installation of new plugin :: Kaplan Plugin
- Install the plugin
- Latest plugin version :: 2015120701

Assumptions

- Follow instructions here to enable and create tokens here (https://docs.moodle.org/27/en/Using_web_services )
- External functions related to block_kaplan_plugin that needs to be exposed for the webservice
1) block_kaplan_plugin_get_users_custom
2) block_kaplan_plugin_get_courses_custom
- Save 'token' related to service under kaplan_plugin settings page

Kaplan Plugin Structure

blocks
++ kaplan_plugin
    ++ db
        - access.php
        - services.php
    ++ lang
        ++ en
            - block_kaplan_plugin.php
    - block_kaplan_plugin.php
    - curl.php
    - edit_form.php
    - externallib.php
    - settings.php
    - test_client.php
    - version.php

-> /db/services.php

    Define webservice functions to install for kaplan_plugin

-> /db/access.php

    Define default capabilities for block

-> /lang/en/block_kaplan_plugin.php

    Define language strings specific to kaplan_plugin block

-> block_kaplan_plugin.php

    Functions implemented for display of this block

-> curl.php

    Functions implemented related to REST webservice

-> externallib.php

    Implementation of external functions get_users_custom and get_courses_custom

-> settings.php

    External page related to block. Config settings 'token' should be saved here. If the token field is empty webservice will not work

-> test_client.php

    Acts as REST client. Makes REST call to external functions of Moodle. Display 'Users List' and 'Courses List' in Moodle.


References

- https://github.com/moodlehq/moodle-local_wstemplate >> Moodle webservice template
- https://docs.moodle.org/27/en/Using_web_services >> How to configure webservice in Moodle
- https://github.com/danielneis/moodle-block_newblock >> Moodle block template